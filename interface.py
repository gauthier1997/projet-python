from tkinter import *
from tkinter import ttk
from user_managment import *
from MdpHashe import *


def callback(*args):
    return args

# fonction d'affichage du mot de passe généré aléatoirement
def displaygenerate(idf):
    current_user = User()

    dispgene = Tk()
    dispgene.title("new password")
    dispgene.geometry("100x100")

    genepwd = genMdp(8)

    Label(dispgene, text=genepwd).grid(row=0)
    current_user.updatepassword(idf, hashMdp(genepwd))

#fonction d'affichage du formulaire de changement d'email
def modifemailwindows(idf):
    current_user = User()
    memailwindows = Tk()
    memailwindows.title("modification email")
    memailwindows.geometry("200x200")

    Label(memailwindows, text="New Email").grid(row=0)
    e1 = Entry(memailwindows)
    e1.grid(row=0, column=1)
    valid = Button(memailwindows, text="Valider", command= lambda :current_user.updateemail(idf, e1.get()))
    valid.grid(row=1, column=1)

#fonction d'affichage du formulaire de changement de mot de passe
def modifpwdwindows(idf):
   current_user = User()
   mpwdwindows = Tk()
   mpwdwindows.title("modification mot de passe")
   mpwdwindows.geometry("200x200")
   Label(mpwdwindows, text="New password").grid(row=0)
   e1 = Entry(mpwdwindows)
   e1.grid(row=0, column=1)

   valid = Button(mpwdwindows, text="valider", command= lambda :current_user.updatepassword(idf, hashMdp(e1.get())))
   valid.grid(row=1, column=1)

   generate = Button(mpwdwindows, text="Generate Password", command= lambda :displaygenerate(idf))
   generate.grid(row=2, column=1)

#fonction d'affichage des utilsateurs sous forme de liste mutlicolonnes
def displayuser():
    duwindows = Tk()
    duwindows.title("Liste des utilisateurs")
    duwindows.geometry('1100x350')

    tree = ttk.Treeview(duwindows, columns=("ID", "Nom", "Prenom", "Email"))
    tree.heading("ID", text="ID")
    tree.heading("Nom", text="Nom")
    tree.heading("Prenom", text="Prenom")
    tree.heading("Email", text="Email")
    tree.pack(padx=10, pady=10)

    user_list_name = displayallusers(1)
    user_list_surname = displayallusers(2)
    user_list_email = displayallusers(3)
    user_id = displayallusers(0)
    cpt = 0
    for w, x, y, z in zip(user_id, user_list_name, user_list_surname, user_list_email):
        tree.insert("", cpt, text="record", values=(w, x, y, z))
        cpt += 1

#fonction d'affichage et de séléction d'utilisateur en vue d'une modification
def modifyuser():
    muwindows = Tk()
    muwindows.title("Modifier un utilisateur")
    muwindows.geometry('1100x350')
    current_user = User()

    tree = ttk.Treeview(muwindows, columns=("ID", "Nom", "Prenom", "Email"))
    tree.heading("ID", text="ID")
    tree.heading("Nom", text="Nom")
    tree.heading("Prenom", text="Prenom")
    tree.heading("Email", text="Email")
    tree.pack(padx=10, pady=10)

    user_list_name = displayallusers(1)
    user_list_surname = displayallusers(2)
    user_list_email = displayallusers(3)
    user_id = displayallusers(0)
    cpt = 0
    for w, x, y, z in zip(user_id, user_list_name, user_list_surname, user_list_email):
        tree.insert("", cpt, text="record", values=(w, x, y, z))
        cpt += 1

    modifemail_button = Button(muwindows, text="Modifier email", command=lambda: modifemailwindows(tree.item(tree.selection()[0])["values"][0]))
    modifpwd_button = Button(muwindows, text="Modifier mot de passe", command=lambda: modifpwdwindows(tree.item(tree.selection()[0])["values"][0]))
    deluser_button = Button(muwindows, text="Supprimer un utilisateur", command=lambda :current_user.deleteuser(tree.item(tree.selection()[0])["values"][0]))
    modifemail_button.pack()
    modifpwd_button.pack()
    deluser_button.pack()

#fonction d'affichage du formulaire de création d'utilisateur
def createuser():
    cuwindows = Tk()
    cuwindows.title("Ajout d'un utilisateur")
    cuwindows.geometry('200x200')
    new_user = User()

    Label(cuwindows, text="First Name").grid(row=0)
    Label(cuwindows, text="Last Name").grid(row=1)
    Label(cuwindows, text="Email").grid(row=2)
    Label(cuwindows, text="Password").grid(row=4)
    Label(cuwindows, text="Confirm").grid(row=5)
    Label(cuwindows, text="Administrateur").grid(row=6)

    # name
    e1 = Entry(cuwindows)

    # lastname
    e2 = Entry(cuwindows)

    # email
    e3 = Entry(cuwindows)

    # password
    e4 = Entry(cuwindows, show="*")

    # confirm
    e5 = Entry(cuwindows, show="*")

    admin = IntVar()

    e6 = Entry(cuwindows)
    e6.insert(END, 0)

    e1.grid(row=0, column=1)
    e2.grid(row=1, column=1)
    e3.grid(row=2, column=1)
    e4.grid(row=4, column=1)
    e5.grid(row=5, column=1)
    e6.grid(row=6, column=1)

    valid = Button(cuwindows, text="valider", command=lambda: new_user.adduser(e1.get(), e2.get(), e3.get(), hashMdp(e4.get()), e6.get(), 0))
    valid.grid(row=7, column=1)

def rootwindows():

    #affichage de la fenêtre principale
    rootwindows = Tk()
    rootwindows.title("Service de gestion d'annuaire")
    rootwindows.geometry('100x120')

    label_title = Label(rootwindows, text="Service de gestion d'annuaire")
    create_user = Button(rootwindows, text='Ajouter un utilisateur', command=createuser)
    list_user = Button(rootwindows, text='liste des utilisateurs ', command=displayuser)
    modify_user = Button(rootwindows, text='Modifier un utilisateur', command=lambda: modifyuser())
    quit_button = Button(rootwindows, text="quitter", command=lambda : rootwindows.quit())

    label_title.pack()
    create_user.pack()
    list_user.pack()
    modify_user.pack()
    quit_button.pack()

    rootwindows.mainloop()


#rootwindows()