import random
import hashlib
import string

#permet de générer un mot de passe manière aléatoire
def genMdp(nbchar):
	charList=string.ascii_letters+string.digits+string.punctuation
	pwd=""

	for i in range(nbchar):
		pwd+=charList[random.randint(0,len(charList)-1)]

	return(pwd)

#permet de rendre le mot de passe généré aléatoirement haché
def hashMdp(pwd):
	salt="un salt"
	hashmdp=hashlib.md5(pwd.encode()+salt.encode()).hexdigest()
	return(hashmdp)

#permet de vérifier si l'utilisateur a saisi le même mot de passe généré
def verifMdp(inputPwd,generatedPwd):
	hashToTest=hashMdp(inputPwd)
	if(hashToTest == generatedPwd):
		return(True)
	else:
		return(False)



#nbcharMdp=input("Combien de caracteres doit faire le mot de passe a générer?\n")
#mdp=genMdp(int(nbcharMdp))
#print("Le mot de passe est : ",mdp)
#hashedPwd=hashMdp(mdp)

#inputPwd=input("Entrer le mot de passe\n")
#if(verifMdp(inputPwd,hashedPwd)==True):
#	print("Mot de passe correct\n")
#else:
#	print("Mot de passe incorrect !\n")
