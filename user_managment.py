from database import connectdb


class User:
    def __init__(self):
        __instance = None

    def selectuser(self, email):
        db = connectdb()

        crs = db.cursor()

        sql = "SELECT password FROM users WHERE users.email = %s"
        crs.execute(sql, (email,))

        result = crs.fetchone()
        print(result[0])

        return result[0]

    def adduser(self, name, surname, email, password, is_admin, login_atempt):
        db = connectdb()

        crs = db.cursor()

        sql = "INSERT INTO users (name, surname, email, password, is_admin, login_atempt) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (name, surname, email, password, is_admin, login_atempt)
        crs.execute(sql, val)
        print(name, surname, email, password, is_admin, login_atempt)

        db.commit()

        print(crs.rowcount, "record inserted.")

    def deleteuser(self, idf):

        db = connectdb()

        crs = db.cursor()

        sql = "DELETE FROM users WHERE users.id=%s"
        val = idf
        crs.execute(sql, (val,))

        db.commit()

    def updateemail(self, idf, newemail):

        db = connectdb()

        crs = db.cursor()

        sql = "UPDATE users SET email = %s WHERE users.id = %s"
        crs.execute(sql, (newemail, idf))
        
        db.commit()

    def updatepassword(self, idf, newpassword):

        db = connectdb()

        crs = db.cursor()

        sql = "UPDATE users SET password = %s WHERE users.id = %s"
        crs.execute(sql, (newpassword, idf))

        db.commit()

    def displayuser(self, idf):

        db = connectdb()

        crs = db.cursor()
        sql = "SELECT id, name, surname  FROM users WHERE users.id = %s"
        val = idf

        crs.execute(sql, (val,))

        results = crs.fetchall()
        print(results)


def displayallusers(index):
    db = connectdb()

    crs = db.cursor()

    sql = "SELECT id, name, surname, email  FROM users"
    crs.execute(sql)

    results = crs.fetchall()

    list_user = []

    for x in results:
        list_user.append(x[index])

    #print(list_user)
    return(list_user)

def get_nb_row():
    db = connectdb()

    crs = db.cursor()
    crs.execute("SELECT COUNT(*) FROM USERS")

    results = crs.fetchone()

    return results[0]

#get_nb_row()


#displayallusers(2)
#new_user = User()
#new_user.adduser('gauthier', 'hubert', 'hubertgauthier5@gmail.com', 'pass@1234', 1, 0)


#new_user.updateemail(1, "test@test")


